#!/bin/sh
### BEGIN INIT INFO
# Provides:          ensembl
# Required-Start:    $local_fs $remote_fs $network $syslog
# Required-Stop:     $local_fs $remote_fs $network $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# X-Interactive:     true
# Short-Description: Start/stop ensembl web server
# Description:       Ensembl is a joint project of the Sanger Center and the European
#                    Bioinformatics Institute, an outstation of the European Molecular
#                    Biology Laboratory, (EMBL-EBI) that are sharing a campus in Hinxton
#                    near Cambridge, UK. It presents the sequence data for the
#                    yet available complete genomes of many vertebrates and is helped
#                    by many sister-projects to cover also plants, invertebrates
#                    and bacteria.
### END INIT INFO

set -e

ENSEMBL_CONFDIR=/usr/share/ensembl

. /lib/lsb/init-functions

PIDFILE=/var/run/ensembl.pid
export APACHE_PID_FILE=$PIDFILE
APACHE2CTL="/usr/sbin/apache2ctl -d $ENSEMBL_CONFDIR -f $ENSEMBL_CONFDIR/conf/httpd.conf -k"

pidof_ensembl() {
	# if there is actually an apache2-ensembl process whose pid is in PIDFILE,
	# print it and return 0.
	if [ -e "$PIDFILE" ]; then
		if pidof apache2 | tr ' ' '\n' | grep -w $(cat $PIDFILE); then
			return 0
		fi
	fi
	return 1
}

ensembl_stop() {
	if $APACHE2CTL configtest > /dev/null 2>&1; then
		# if the config is ok than we just stop normaly
                $APACHE2CTL stop 2>&1 | grep -v 'not running' >&2 || true
	else
		# if we are here something is broken and we need to try
		# to exit as nice and clean as possible
		PID=$(pidof_ensembl) || true

		if [ "${PID}" ]; then
			# in this case it is everything nice and dandy and we kill apache2
			echo
			log_warning_msg "The ensembl configtest failed, so we are trying to kill it manually. This is almost certainly suboptimal, so please make sure your system is working as you'd expect now!"
                        kill $PID
		elif [ "$(pidof apache2)" ]; then
			if [ "$VERBOSE" != no ]; then
                                echo " ... failed!"
			        echo "You may still have some ensembl processes running.  There are"
 			        echo "processes named 'apache2' which do not match your pid file,"
			        echo "and in the name of safety, we've left them alone.  Please review"
			        echo "the situation by hand."
                        fi
                        return 1
		fi
	fi
}

ensembl_wait_stop() {
	# running ?
	PIDTMP=$(pidof_ensembl) || true
	if kill -0 "${PIDTMP:-}" 2> /dev/null; then
	    PID=$PIDTMP
	fi

	ensembl_stop

	# wait until really stopped
	if [ -n "${PID:-}" ]; then
		i=0
		while kill -0 "${PID:-}" 2> /dev/null;  do
        		if [ $i = '60' ]; then
        			break;
        	 	else
        			if [ $i = '0' ]; then
                			echo -n " ... waiting "
        			else
                	      		echo -n "."
        		 	fi
        			i=$(($i+1))
        			sleep 1
        	      fi
		 done
	fi
}

case $1 in
	start)
		# /var/log/ensembl/logs         # this is a symlink set by the Debian package
		# /var/cache/ensembl/img-cache  # this is set as a symlink further down
		# /var/tmp/ensembl/img-tmp      # another symlink set further down
		# /var/tmp/ensembl/tmp          # and yet another one 
		#for d in /usr/share/ensembl/tmp  /var/cache/ensembl
		#do
		#	if [ ! -d "$d" ]; then mkdir $d; fi
		#	chown nobody:nogroup -R "$d"
		# 	chmod 755 "$d"
		#done

		echo "FIXME: In case something might went wrong you need to run '/etc/init.d/apache2 stop' first.  Please report this to Debian Maintainer of ensembl." >&2
		log_daemon_msg "Starting web server" "ensembl"
		if $APACHE2CTL start; then
                        log_end_msg 0
		else
                        log_end_msg 1
                fi
	;;
	stop)
		log_daemon_msg "Stopping web server" "ensembl"
		if ensembl_wait_stop; then
                        log_end_msg 0
                else
                        log_end_msg 1
                fi
	;;
	graceful-stop)
		log_daemon_msg "Stopping web server" "ensembl"
		if $APACHE2CTL graceful-stop; then
                        log_end_msg 0
                else
                        log_end_msg 1
                fi
	;;
	reload | force-reload)
		if ! $APACHE2CTL configtest > /dev/null 2>&1; then
                    $APACHE2CTL configtest || true
                    log_end_msg 1
                    exit 1
                fi
                log_daemon_msg "Reloading web server config" "ensembl"
		if pidof_ensembl > /dev/null ; then
                    if $APACHE2CTL graceful $2 ; then
                        log_end_msg 0
                    else
                        log_end_msg 1
                    fi
                fi
	;;
	restart)
		if ! $APACHE2CTL configtest > /dev/null 2>&1; then
		    $APACHE2CTL configtest || true
		    log_end_msg 1
		    exit 1
		fi
		log_daemon_msg "Restarting web server" "ensembl"
		PID=$(pidof_ensembl) || true
		if ! ensembl_wait_stop; then
                        log_end_msg 1 || true
                fi
		if $APACHE2CTL start; then
                        log_end_msg 0
                else
                        log_end_msg 1
                fi
	;;
	status)
		PID=$(pidof_ensembl) || true
		if [ -n "$PID" ]; then
			echo "Ensembl is running (pid $PID)."
			exit 0
		else
			echo "Ensembl is NOT running."
			exit 1
		fi
	;;
	*)
		log_success_msg "Usage: /etc/init.d/ensembl {start|stop|graceful-stop|restart|reload|force-reload|status}"
		exit 1
	;;
esac
