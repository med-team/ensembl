## If you wish to use the EnsEMBL web-code from the command line, you will
## need to hardcode the server root here 

$SiteDefs::ENSEMBL_SERVERROOT = '/usr/share/ensembl';

$SiteDefs::ENSEMBL_PLUGINS = [
  'EnsEMBL::Mirror'    => $SiteDefs::ENSEMBL_SERVERROOT.'/public-plugins/mirror',
  'EnsEMBL::Ensembl'   => $SiteDefs::ENSEMBL_SERVERROOT.'/public-plugins/ensembl'
];

1;
