#!/bin/bash

for i in "$@"
do
	if head -n 1 $i | grep -q '^#!'; then 
		chmod 755 "$i"
	fi
done
