#!/bin/bash

VERBOSE=false

for i in "$@"
do

	if head -n 1 "$i" | egrep -q '^#!\s*(/usr/bin/perl(\s+-w)?|/bin/bash|/usr/bin/wish)\s*$'; then 
		if echo "$i" | egrep -q '.pm$'; then
			echo "Remove: '$i'"
			sed -i '1d' $i
			chmod -x "$i"
		else
			chmod 755 "$i"
			if [ "$VERBOSE" = "true" ] ; then echo "OK:         '$i'" ; fi
		fi
		continue
	elif echo "$i" | egrep -q '.pm$'; then
		if [ "$VERBOSE" = "true" ] ; then echo "OK:         '$i'" ; fi
		chmod 644 "$i"
		continue
	fi

	if head -n 1 "$1" | grep -q '^#!'; then
		if echo "$i" | egrep -q '.pm$'; then
			echo "This script should have performed the removal for .pm files already."
			exit -1
		elif echo "$i" | egrep -q '*.t$'; then
			if [ ! -x "$i" ]; then
				if [ "$VERBOSE" = "true" ] ; then echo "Remove for non-executable .t:  '$i'" ; fi
				# not executable, should then lose shebang line
				# well, it should not have one in the first place
				sed -i '1s%^#!.*$%%' $i
				continue
			fi
		fi
		if [ "$VERBOSE" = "true" ] ; then echo "Substitute: '$i'" ; fi
		if head -n 1 "$i" | grep -q perl; then
			if [ "$VERBOSE" = "true" ] ; then echo Perl ; fi
			# remove version from Perl
			sed -i '1s%perl5[0-9.]*%perl%' $i
			# now the path substitution, keeping arguments to Perl intact
			sed -i '1s%#!\s*/.*/perl%#!/usr/bin/perl%' $i
			# also seen
			sed -i '1s%#![[:space:]]*perl%#!/usr/bin/perl%' $i
		elif head -n 1 "$i" | grep -q wish; then
			if [ "$VERBOSE" = "true" ] ; then echo Tcl/Tk ; fi
			sed -i '1s%#!\s*/.*/wish.*%#!/usr/bin/wish%' $i
		else
			if [ "$VERBOSE" = "true" ] ; then echo BASH ; fi
			sed -i '1s%#!\s*/.*/bash%#!/bin/bash%' $i
		fi
		chmod 755 "$i"
	else
		if echo "$i" | egrep -q '*.t$'; then
			if [ ! -x "$i" ]; then
				if [ "$VERBOSE" = "true" ] ; then echo "Leave without:     '$i'" ; fi
				# not executable, should then lose shebang line
				# well, it should not have one in the first place
				sed -i '1s%^#!.*$%%' $i
				continue
			fi
		fi 
		if [ "$VERBOSE" = "true" ] ; then echo "Insert:     '$i'" ; fi
		if echo "$i" | egrep -q '.(pl|t)$' || egrep -q "use\s\s*strict\s*;" $i; then
			sed -i '1i#!/usr/bin/perl\n' $i
			chmod 755 "$i"
		elif (echo "$i" | grep -q ".tcl" ) ; then
			sed -i '1i#!/usr/bin/wish\n' $i
			chmod 755 "$i"
		elif (echo "$i" | grep -q ".sh" ) ; then
			sed -i '1i#!/bin/bash\n' $i
			chmod 755 "$i"
		else
			echo "          * '$i'"
		fi
	fi
done
