#
# Regular cron jobs for the ensembl package
#	the web server creates many temporary images which should be removed
#	on a regular basis
#
#0 4	* * *	root	[ -x /usr/bin/ensembl_maintenance ] && /usr/bin/ensembl_maintenance
